# Workflow Engine Example

This demonstrates the behaviour of the django-planner using a hypotentical Widget production line. 

Widgets have the following characteristics:

* Each widget is produced and manufactured in the same way. 
* Each widget has a unique uuid which is used to track the widget through the process
* Each widget is assigned a unique distribution uuid once it is packaged so it can be identified by distributors.
* Each widget is painted in a color defined by the user..
* Each widget must be engraved with a word which is randomly generated during the production process.

Production of a Widget from manufacturing to distribution involves the following steps:

1. Creation of the widget
	* Assemble the parts
    * Randomly generate the a word with which to engrave the widget
	* Engrave the widget with the word letter by letter
		- Engrave first letter
		...
		- Engrave the last letter
	* Paint it the predefined colour
2. Package the widget in a box
3. Send the package 


The manufacturing process uses a revolutionary "logging" technique. To complete each of the steps above the following must be logged to stdout:

* Assemble the parts - "Assembling parts for widget uuid={widget_uuid}"
* Generating random word - "Generated random word {random_word}"
* Engrave the widget with name letter by letter - "Engrave widget uuid={widget_uuid} with word={random_word} "
* Engrave the widget with letter - "Engraving widget uuid={widget_uuid} with letter={letter}"
* Paint it the predefined color - "Painting widget uuid={widget_uuid} color={color}"
* Packaging the widget - "Packaging widget uuid={widget_uuid} into package with uuid={package_uuid}"
* Send the package - "Sending package uuid={package_uuid}" 


Additional Requirements:

* Widgets can only be produced when there is an active WiFi connection.
* Each step must be completed before the subsequent step can be done
* All child/sub-steps must be completed before a parent step can be considered to be complete.

### Features of this process

* Widget UUID is generated in the first step, not ahead of time.
* Sending the package requires a package UUID which is generated in the previous step - state must be passed through the system.
* The steps for the entire production line are not known up-front - Once the random word is generated during creation of the widget, extra steps will need to be created to stamp the letters.
* Execution is automatically stopped if you disconnect the host machine from the internet - this simulates a health check on a supporting service/third party dependency.


# Quickstart

You can quickly run this example by using docker-compose. Simply run

```bash
$ docker-compose build
```

```bash
$ docker-compose up
```

Create a superuser 
```bash
$ docker-compose exec django python manage.py createsupersuer 
```

Log into the Django Admin and create WorkflowExecutor object in the database with the name `widget-executor`.

Visit `localhost:8000` and create a widget of a given color using the form and the celery queue will process that widget. 

## Extra details

* If you pause the widget executor during production then widget production will automatically stop.
* If you turn on the widget executor during prodicution then widget production will start again.
* If your internet connection is disconnected, then widget production will automatically stop.
* If you re-enable your internet connection, then widget production will automatically resume.
* If you change one of the operator methods to raise an Error then next time you run the workflow the workflow engine will automatically stop further processing.
