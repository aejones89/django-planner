from django.http import HttpResponse
from django.shortcuts import render
from planner.models import WorkflowExecutor

from example.widgets.services.produce_widget import make_widget


def create_widget_view(request):
    context = {'widget_executor_exists': False}
    executor = WorkflowExecutor.objects.filter(name='widget-executor')

    if executor.exists():
        context['widget_executor_exists'] = True

    if request.method == "POST":
        color = request.POST.get('color')
        make_widget(color)
        context.update({'color': color})
    return render(request, 'create_widget.html', context)
