import urllib.request
import logging

from django.db import models
from planner.models import WorkflowExecutor

logger = logging.getLogger(__name__)


class CustomWorkflowExecutorManager(models.Manager):

    @classmethod
    def refresh(cls):
        widget_workflow = WorkflowExecutor.objects.get(name='widget-executor')
        try:
            urllib.request.urlopen("http://google.com")
            if not widget_workflow.is_on:
                widget_workflow.resume(reason='Wifi connection re-estabished.')
        except:
            logging.warning("Pausing Workflow engine")
            widget_workflow.pause(reason='No wifi connection was found. Pausing executor.')



class CustomWorkflowExecutor(WorkflowExecutor):
    """Extent the WorkflowExecutor model with a custom manager to refresh every 5 seconds."""

    objects = CustomWorkflowExecutorManager()

    class Meta:
        proxy = True