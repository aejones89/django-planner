from datetime import timedelta

from celery.decorators import periodic_task

from production.models import CustomWorkflowExecutor


@periodic_task(run_every=timedelta(seconds=10))
def refresh_widget_executor():
    CustomWorkflowExecutor.objects.refresh()
