#!/usr/bin/env bash

pip install -e /django_planner_lib

celery -A widgets worker -l info --concurrency=4
