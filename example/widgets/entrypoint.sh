#!/usr/bin/env bash

pip install -e /django_planner_lib

python manage.py migrate

python manage.py runserver 0.0.0.0:8000