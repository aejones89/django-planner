import logging
import uuid
import random
import string

from planner.operators import push_root_context, pull_root_context
from planner.engine import WorkflowStep

from example.widgets.services import steps

logger = logging.getLogger(__name__)


def random_sleep(seconds_override=None):
    import time
    if seconds_override:
        time.sleep(seconds_override)
    else:
        time.sleep(random.randint(0, 5))


def produce_widget_operator(payload, step):
    random_sleep()
    color = payload['color']
    logger.info(
        f"Producing Widget - color={color}"
    )


def create_widget_operator(payload, step):
    random_sleep()
    widget_uuid = uuid.uuid4()
    push_root_context(step, {'widget_uuid': str(widget_uuid)})
    logger.info(
        f"Creating widget - uuid={widget_uuid}"
    )


def assemble_parts_operator(payload,step):
    random_sleep()
    context = pull_root_context(step)
    widget_uuid = context['widget_uuid']
    logger.info(
        f"Assembled the parts for widget - uuid={widget_uuid}"
    )


def randomly_generate_word_operator(payload, step):
    random_sleep()
    length = random.randint(1, 15)
    random_word = ''.join(random.choice(string.ascii_lowercase) for _ in range(length))
    push_root_context(step, {'random_word': random_word})

    logger.info(
        f"Generated random word {random_word}"
    )


def create_engrave_workflow_for_widget_operator(payload, step):
    random_sleep()
    context = pull_root_context(step)
    random_word = context['random_word']
    widget_uuid = context['widget_uuid']

    for letter in random_word:
        WorkflowStep.objects.create(
            name=f"Engrave letter - widget_uuid={widget_uuid}, letter={letter}",
            details="Engrave widget with letter",
            executor_name=step.executor_name,
            parent=step,
            step_type=steps.ENGRAVE_WIDGET_WITH_LETTER,
            payload={'widget_uuid': widget_uuid, 'letter': letter},
        )

    logger.info(
        f"Engrave widget with word - widget_uuid={widget_uuid}, word={random_word}"
    )


def engrave_widget_with_letter_operator(payload, step):
    random_sleep(1)
    context = pull_root_context(step)
    widget_uuid = context['widget_uuid']
    letter = payload['letter']

    logger.info(
        f"Engraving widget - widget_uuid={widget_uuid}, letter={letter}"
    )



def paint_widget_operator(payload, step):
    random_sleep()
    context = pull_root_context(step)
    widget_uuid = context['widget_uuid']
    color = payload['color']
    logger.info(
        f"Paint widget - widget_uuid={widget_uuid}, color={color}"
    )


def package_widget_operator(payload, step):
    random_sleep()
    context = pull_root_context(step)
    widget_uuid = context['widget_uuid']

    package_uuid = uuid.uuid4()
    push_root_context(step, {'package_uuid': str(package_uuid)})

    logger.info(
        f"Packaging widget - widget_uuid={widget_uuid}, package_uuid={package_uuid}"
    )


def send_package_operator(payload, step):
    random_sleep()
    context = pull_root_context(step)
    package_uuid = context['package_uuid']
    logger.info(
        f"Sending package - package_uuid={package_uuid}"
    )
