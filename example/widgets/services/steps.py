from example.widgets.services import operators

PRODUCE_WIDGET = 'produce_widget'
CREATE_WIDGET = 'create_widget'
ASSEMBLE_PARTS = 'assemble_parts'
RANDOMLY_GENERATE_WORD = 'randomly_generate_word'
CREATE_ENGRAVE_WORKFLOW = 'create_engrave_workflow'
ENGRAVE_WIDGET_WITH_WORD = 'engrave_widget_with_word'
ENGRAVE_WIDGET_WITH_LETTER = 'engrave_widget_with_letter'
PAINT_WIDGET = 'paint_widget'
PACKAGE_WIDGET = 'package_widget'
SEND_PACKAGE = 'send_package'

STEPS = {
    PRODUCE_WIDGET: operators.produce_widget_operator,
    CREATE_WIDGET: operators.create_widget_operator,
    ASSEMBLE_PARTS: operators.assemble_parts_operator,
    RANDOMLY_GENERATE_WORD: operators.randomly_generate_word_operator,
    CREATE_ENGRAVE_WORKFLOW: operators.create_engrave_workflow_for_widget_operator,
    ENGRAVE_WIDGET_WITH_LETTER: operators.engrave_widget_with_letter_operator,
    PAINT_WIDGET: operators.paint_widget_operator,
    PACKAGE_WIDGET: operators.package_widget_operator,
    SEND_PACKAGE: operators.send_package_operator,
}
