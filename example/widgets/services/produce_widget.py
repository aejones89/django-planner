from example.widgets.services import steps
from example.widgets.services.executors import ExecutorRegistry
from planner.engine import WorkflowPlan
from planner.models import WorkflowExecutor, WorkflowStep


def make_widget(color: str):
    widget_executor_name = ExecutorRegistry.widget_executor.value
    executor = WorkflowExecutor.objects.get(name=widget_executor_name)

    with WorkflowPlan(
        name=f"Produce widget of color={color}",
        details='Produce a widget of a given color',
        step_type=steps.PRODUCE_WIDGET,
        executor_name=executor.name,
        payload={'color': color},
    ) as produce_widget_workflow:

        with WorkflowPlan(
            name=f"Create widget with color={color}",
            details="Create the widget",
            step_type=steps.CREATE_WIDGET,
            executor_name=produce_widget_workflow.executor_name,
            payload={},
        ) as create_widget_workflow:

            produce_widget_workflow.add_step(create_widget_workflow.step)

            assemble_step = WorkflowStep.objects.create(
                name="Assemble parts for widget",
                details="Assemble the parts for a widget",
                step_type=steps.ASSEMBLE_PARTS,
            )
            create_widget_workflow.add_step(assemble_step)

            generate_random_word_step = WorkflowStep.objects.create(
                name="Generate random word for widget",
                details="Generate the random word for a widget",
                step_type=steps.RANDOMLY_GENERATE_WORD,
            )
            create_widget_workflow.add_step(generate_random_word_step)

            create_engrave_workflow = WorkflowStep.objects.create(
                name="Create the workflow steps to engrave widget",
                details="Create the workflow steps to engrave widget",
                step_type=steps.CREATE_ENGRAVE_WORKFLOW,
            )
            create_widget_workflow.add_step(create_engrave_workflow)

            paint_step = WorkflowStep.objects.create(
                name=f"Paint widget with color={color}",
                details="Paint the widget",
                step_type=steps.PAINT_WIDGET,
                payload={'color': color},
            )
            create_widget_workflow.add_step(paint_step)

        package_widget_step = WorkflowStep.objects.create(
            name="Package the widget",
            details="Package widget and generate package uuid",
            step_type=steps.PACKAGE_WIDGET,
        )
        produce_widget_workflow.add_step(package_widget_step)

        send_package_step = WorkflowStep.objects.create(
            name="Send package",
            details="Send the package of a given uuid",
            step_type=steps.SEND_PACKAGE,
        )
        produce_widget_workflow.add_step(send_package_step)