from unittest import mock

import pytest

from planner.engine import WorkflowPlan
from planner.models import WorkflowStep


@pytest.mark.django_db
class TestWorkflow:
    @mock.patch("planner.engine.run_executor_task")
    def test_workflow_creation(self, mock_run_executor):
        with WorkflowPlan(
            name="test-name",
            details="test-details",
            step_type="test-step",
            executor_name="test-executor",
            payload={"key": "value"},
        ) as test_workflow:
            pass

        step = WorkflowStep.objects.first()

        assert step.name == "test-name"
        assert step.payload == {"key": "value"}
        mock_run_executor.delay.assert_called_once()

    @mock.patch("planner.engine.run_executor_task")
    def test_workflow_add_step(self, mock_run_executor):
        with WorkflowPlan(
            name="one",
            details="test-details",
            step_type="test-step",
            executor_name="test-executor",
            payload={"key": "value"},
        ) as test_workflow:
            pass

        step = WorkflowStep.objects.create(
            name="two",
            details="test details",
            status="planned",
            payload={"key": "value"},
            step_type="test step",
        )
        test_workflow.add_step(step)

        first_step = WorkflowStep.objects.get(name="one")
        second_step = WorkflowStep.objects.get(name="two")

        assert second_step.parent == first_step
        assert second_step.executor_name == first_step.executor_name

    @mock.patch("planner.engine.run_executor_task")
    def test_nested_workflow(self, mock_run_executor):
        with WorkflowPlan(
            name="one",
            details="test-details",
            step_type="test-step",
            executor_name="test-executor",
            payload={"key": "value"},
        ) as wf_one:

            with WorkflowPlan(
                name="two",
                details="test-details",
                step_type="test-step",
                executor_name="test-executor",
                payload={"key": "value"},
            ) as wf_two:
                wf_one.add_step(wf_two.step)

        step_one = WorkflowStep.objects.first()
        step_two = WorkflowStep.objects.last()

        assert step_two.parent == step_one

    @mock.patch("planner.engine.run_executor_task")
    @mock.patch("planner.engine.WorkflowPlan.add_step", side_effect=Exception)
    def test_workflow_is_atomic(self, mock_run_executor, mock_add_step):
        try:
            with WorkflowPlan(
                name="atomic one",
                details="test-details",
                step_type="test-step",
                executor_name="test-executor",
                payload={"key": "value"},
            ) as wf_one:
                with WorkflowPlan(
                    name="atomic two",
                    details="test-details",
                    step_type="test-step",
                    executor_name="test-executor",
                    payload={"key": "value"},
                ) as wf_two:
                    wf_one.add_step(wf_two.step)
        except Exception:
            pass

        steps = WorkflowStep.objects.all()
        assert steps.count() == 0
