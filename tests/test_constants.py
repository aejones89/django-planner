import pytest

from planner.models import WorkflowStepStatus


def test_workflow_step_status_values():
    values = WorkflowStepStatus

    expected_attributes = ["FAILED", "IN_PROGRESS", "PLANNED", "SUCCESSFUL"]
    expected_values = ["failed", "in_progress", "planned", "successful"]

    for a, v in zip(expected_attributes, expected_values):
        assert getattr(values, a) == v


def test_workflow_executor_status_values():
    values = WorkflowStepStatus

    expected_attributes = ["FAILED", "IN_PROGRESS", "PLANNED", "SUCCESSFUL"]
    expected_values = ["failed", "in_progress", "planned", "successful"]

    for a, v in zip(expected_attributes, expected_values):
        assert getattr(values, a) == v
