from unittest import mock

import pytest
from django.test import override_settings

from planner.engine import WorkflowPlan
from planner.models import WorkflowExecutor, WorkflowStep
from planner.tasks import run_executor


@pytest.mark.django_db
class TestIntegrations:
    @override_settings(STEPS_MAPPING="tests.fixtures.successful_workflow.STEPS")
    @mock.patch("planner.engine.run_executor_task.delay", side_effect=run_executor)
    def test_steps_are_processed(self, mock_run_executor):
        executor = WorkflowExecutor.objects.create(name="test-executor")
        with WorkflowPlan(
            name="parent-step",
            details="test-details",
            step_type="parent_step",
            executor_name=executor.name,
            payload={"key": "value"},
        ) as test_wf:
            step_one = WorkflowStep.objects.create(
                name="child-step-one",
                details="test details",
                payload={"key": "value"},
                step_type="step_one",
            )
            test_wf.add_step(step_one)

            step_two = WorkflowStep.objects.create(
                name="child-step-two",
                details="test details",
                payload={"key": "value"},
                step_type="step_two",
            )
            test_wf.add_step(step_two)

        steps = WorkflowStep.objects.all()

        for step in steps:
            assert step.status == "successful"

    @override_settings(STEPS_MAPPING="tests.fixtures.unsuccessful_workflow.STEPS")
    @mock.patch("planner.engine.run_executor_task.delay", side_effect=run_executor)
    def test_steps_are_marked_as_failed_if_execution_fails(self, mock_run_executor):
        executor = WorkflowExecutor.objects.create(name="test-executor")
        with WorkflowPlan(
            name="parent-step",
            details="test-details",
            step_type="parent_step",
            executor_name=executor.name,
            payload={"key": "value"},
        ) as test_wf:
            step_one = WorkflowStep.objects.create(
                name="child-step-one",
                details="test details",
                payload={"key": "value"},
                step_type="step_one",
            )
            test_wf.add_step(step_one)

            step_two = WorkflowStep.objects.create(
                name="child-step-two",
                details="test details",
                payload={"key": "value"},
                step_type="step_two",
            )
            test_wf.add_step(step_two)

        steps = WorkflowStep.objects.all()
        parent_step = steps.filter(name="parent-step").get()
        child_one_step = steps.filter(name="child-step-one").get()
        child_two_step = steps.filter(name="child-step-two").get()
        executor.refresh_from_db()

        assert parent_step.status == "successful"
        assert child_one_step.status == "failed"
        assert child_two_step.status == "planned"
        assert executor.status == "paused"
