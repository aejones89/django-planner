import pytest

from planner.models import WorkflowStep


@pytest.fixture
def single_planned_workflow():
    parent = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=parent,
            executor_name=parent.executor_name,
        )


@pytest.fixture
def single_successful_workflow():
    parent = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=parent,
            executor_name=parent.executor_name,
            status="successful",
        )


@pytest.fixture
def single_workflow_with_failed():
    parent = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 3):
        WorkflowStep.objects.create(
            name=f"Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=parent,
            executor_name=parent.executor_name,
            status="successful",
        )
    WorkflowStep.objects.create(
        name="Child 3",
        details="Details for Test Step Parent",
        step_type="test_type",
        parent=parent,
        executor_name=parent.executor_name,
        status="failed",
    )
    for i in range(4, 6):
        WorkflowStep.objects.create(
            name=f"Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=parent,
            executor_name=parent.executor_name,
        )


@pytest.fixture
def single_workflow_with_in_progress():
    parent = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 3):
        WorkflowStep.objects.create(
            name=f"Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=parent,
            executor_name=parent.executor_name,
            status="successful",
        )
    WorkflowStep.objects.create(
        name="Child 3",
        details="Details for Test Step Parent",
        step_type="test_type",
        parent=parent,
        executor_name=parent.executor_name,
        status="in_progress",
    )
    for i in range(4, 6):
        WorkflowStep.objects.create(
            name=f"Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=parent,
            executor_name=parent.executor_name,
        )


@pytest.fixture
def multiple_planned_workflow():
    a_parent = WorkflowStep.objects.create(
        name="A - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"A - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=a_parent,
            executor_name=a_parent.executor_name,
        )
    b_parent = WorkflowStep.objects.create(
        name="B - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"B - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=b_parent,
            executor_name=b_parent.executor_name,
        )


@pytest.fixture
def multiple_successful_workflow():
    a_parent = WorkflowStep.objects.create(
        name="A - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"A - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=a_parent,
            executor_name=a_parent.executor_name,
            status="successful",
        )
    b_parent = WorkflowStep.objects.create(
        name="B - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"B - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=b_parent,
            executor_name=b_parent.executor_name,
            status="successful",
        )


@pytest.fixture
def multiple_workflow_with_failed():
    a_parent = WorkflowStep.objects.create(
        name="A - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 4):
        WorkflowStep.objects.create(
            name=f"A - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=a_parent,
            executor_name=a_parent.executor_name,
            status="successful",
        )
    WorkflowStep.objects.create(
        name=f"A - Child 4",
        details="Details for Test Step Parent",
        step_type="test_type",
        parent=a_parent,
        executor_name=a_parent.executor_name,
        status="failed",
    )
    b_parent = WorkflowStep.objects.create(
        name="B - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"B - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=b_parent,
            executor_name=b_parent.executor_name,
        )


@pytest.fixture
def multiple_workflow_with_in_progress():
    a_parent = WorkflowStep.objects.create(
        name="A - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        status="successful",
    )
    for i in range(0, 4):
        WorkflowStep.objects.create(
            name=f"A - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=a_parent,
            executor_name=a_parent.executor_name,
            status="successful",
        )
    WorkflowStep.objects.create(
        name=f"A - Child 4",
        details="Details for Test Step Parent",
        step_type="test_type",
        parent=a_parent,
        executor_name=a_parent.executor_name,
        status="in_progress",
    )
    b_parent = WorkflowStep.objects.create(
        name="B - Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
    )
    for i in range(0, 5):
        WorkflowStep.objects.create(
            name=f"B - Child {i}",
            details="Details for Test Step Parent",
            step_type="test_type",
            parent=b_parent,
            executor_name=b_parent.executor_name,
        )
