from unittest import mock

import pytest
from planner.models import WorkflowStep, WorkflowExecutor


@pytest.mark.django_db
class TestWorkflowStepModel:
    def test_workflow_step_fields(self):
        workflow_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=None,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )

        assert workflow_step.name == "test name"
        assert workflow_step.details == "test details"
        assert workflow_step.parent == None
        assert workflow_step.status == "planned"
        assert workflow_step.payload == {"key": "value"}
        assert workflow_step.context == {"pointer": "result"}
        assert workflow_step.step_type == "test step"
        assert workflow_step.created_at

    def test_step_can_have_parent(self):
        parent_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=None,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )
        child_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=parent_step,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )

        assert child_step.parent is parent_step
        assert parent_step.children.all()[0] == child_step

    def test_mark_step_as_successful(self):
        parent_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=None,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )
        parent_step.mark_successful()
        assert parent_step.status == "successful"

    def test_mark_step_as_failed(self):
        parent_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=None,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )
        parent_step.mark_failed()
        assert parent_step.status == "failed"

    def test_mark_step_as_in_progress(self):
        parent_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=None,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )
        parent_step.mark_in_progress()
        assert parent_step.status == "in_progress"

    def test_start_processing(self):
        parent_step = WorkflowStep.objects.create(
            name="test name",
            details="test details",
            parent=None,
            status="planned",
            payload={"key": "value"},
            context={"pointer": "result"},
            step_type="test step",
        )
        parent_step.start_processing()
        assert parent_step.status == "in_progress"
        assert parent_step.num_attempts == 1


@pytest.mark.django_db
class TestWorkflowExecutorModel:
    def test_workflowexecutor_fields(self):
        executor = WorkflowExecutor.objects.create(name="Test Executor")
        assert executor.status == "on"
        assert executor.last_status_updated_reason == "Created"
        assert executor.last_status_updated_at

    def test_update_status(self):
        executor = WorkflowExecutor.objects.create(name="Test Executor")
        executor._update_status("off", "turning off for testing")

        assert executor.status == "off"
        assert executor.last_status_updated_reason == "turning off for testing"

    def test_pause(self):
        executor = WorkflowExecutor.objects.create(name="Test Executor")
        executor.pause(reason="try pause")
        assert executor.status == "paused"
        assert executor.last_status_updated_reason == "try pause"

    @mock.patch("planner.signals.run_executor_task")
    def test_resume(self, mock_run_executor):
        executor = WorkflowExecutor.objects.create(name="Test Executor")
        executor.resume(reason="try resume")
        assert executor.status == "on"
        assert executor.last_status_updated_reason == "try resume"

    def test_turn_off(self):
        executor = WorkflowExecutor.objects.create(name="Test Executor")
        executor.turn_off(reason="try turn off")
        assert executor.status == "off"
        assert executor.last_status_updated_reason == "try turn off"

    def test_is_on(self):
        executor = WorkflowExecutor.objects.create(name="Test Executor", status="on")
        assert executor.is_on is True
        assert executor.is_off is False
        assert executor.is_paused is False

    def test_is_off(self):
        executor = WorkflowExecutor.objects.create(name="Test Executor", status="off")
        assert executor.is_on is False
        assert executor.is_off is True
        assert executor.is_paused is False

    def test_is_paused(self):
        executor = WorkflowExecutor.objects.create(
            name="Test Executor", status="paused"
        )
        assert executor.is_on is False
        assert executor.is_off is False
        assert executor.is_paused is True


@pytest.mark.django_db
class TestWorkflowStepManager:
    def test_no_planned_tasks(self):
        planned_steps = WorkflowStep.objects.planned_steps(executor_name=None)
        assert planned_steps.count() == 0

    def test_single_workflow_planned(self, single_planned_workflow):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        expected_names = [
            "Parent",
            "Child 0",
            "Child 1",
            "Child 2",
            "Child 3",
            "Child 4",
        ]
        assert expected_names == [s.name for s in planned_steps]
        for s in planned_steps:
            assert s.status == "planned"

    def test_single_workflow_successful(self, single_successful_workflow):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        assert planned_steps.count() == 0

    def test_single_workflow_with_failed(self, single_workflow_with_failed):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        expected_names = ["Child 3", "Child 4", "Child 5"]
        expected_statuses = ["failed", "planned", "planned"]
        assert expected_names == [s.name for s in planned_steps]
        assert expected_statuses == [s.status for s in planned_steps]

    def test_single_workflow_with_in_progress(self, single_workflow_with_in_progress):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        expected_names = ["Child 4", "Child 5"]
        expected_statuses = ["planned", "planned"]
        assert expected_names == [s.name for s in planned_steps]
        assert expected_statuses == [s.status for s in planned_steps]

    def test_multiple_workflow_planned(self, multiple_planned_workflow):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        expected_names = [
            "A - Parent",
            "A - Child 0",
            "A - Child 1",
            "A - Child 2",
            "A - Child 3",
            "A - Child 4",
            "B - Parent",
            "B - Child 0",
            "B - Child 1",
            "B - Child 2",
            "B - Child 3",
            "B - Child 4",
        ]
        assert expected_names == [s.name for s in planned_steps]

    def test_multiple_workflow_planned(self, multiple_successful_workflow):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        assert planned_steps.count() == 0

    def test_multiple_workflow_with_failed(self, multiple_workflow_with_failed):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        expected_names = [
            "A - Child 4",
            "B - Parent",
            "B - Child 0",
            "B - Child 1",
            "B - Child 2",
            "B - Child 3",
            "B - Child 4",
        ]
        expected_statuses = [
            "failed",
            "planned",
            "planned",
            "planned",
            "planned",
            "planned",
            "planned",
        ]
        assert expected_names == [s.name for s in planned_steps]
        assert expected_statuses == [s.status for s in planned_steps]

    def test_multiple_workflow_with_in_progress(
        self, multiple_workflow_with_in_progress
    ):
        planned_steps = WorkflowStep.objects.planned_steps(
            executor_name="test-executor"
        )
        expected_names = [
            "B - Parent",
            "B - Child 0",
            "B - Child 1",
            "B - Child 2",
            "B - Child 3",
            "B - Child 4",
        ]
        expected_statuses = [
            "planned",
            "planned",
            "planned",
            "planned",
            "planned",
            "planned",
        ]
        assert expected_names == [s.name for s in planned_steps]
        assert expected_statuses == [s.status for s in planned_steps]


@pytest.mark.django_db
class TestWorkflowExecutorSignal:
    @mock.patch("planner.signals.run_executor_task")
    def test_run_executor_not_fired_if_created(self, mock_run_executor):
        WorkflowExecutor.objects.create(name="Test Executor")
        mock_run_executor.delay.assert_not_called()

    @mock.patch("planner.signals.run_executor_task")
    def test_run_executor_fired_if_turned_on(self, mock_run_executor):
        executor = WorkflowExecutor.objects.create(name="Test Executor")
        executor.status = "on"
        executor.save()
        mock_run_executor.delay.assert_called_once()

    @mock.patch("planner.signals.run_executor_task")
    def test_run_executor_fired_if_on_and_saved(self, mock_run_executor):
        executor = WorkflowExecutor.objects.create(name="Test Executor", status="on")
        mock_run_executor.delay.assert_not_called()
        executor.name = "new name"
        executor.save()
        mock_run_executor.delay.assert_called_once()
