from planner.models import WorkflowStep
from planner.operators import pull_root_context, push_root_context
import pytest


@pytest.mark.django_db
def test_pull_root_context():
    parent = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        payload={"payload-key": "payload-value"},
        context={"context-key": "context-value"},
    )
    child = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        parent=parent,
    )
    context = pull_root_context(child)

    assert context == {"context-key": "context-value"}


@pytest.mark.django_db
def test_push_root_context():
    parent = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        payload={"payload-key": "payload-value"},
    )
    child = WorkflowStep.objects.create(
        name="Parent",
        details="Details for Test Step Parent",
        step_type="test_type",
        executor_name="test-executor",
        parent=parent,
    )
    context = {"age": 102, "city": "London"}
    push_root_context(child, context)

    parent.refresh_from_db()

    assert parent.context == context
