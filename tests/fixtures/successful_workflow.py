def operator_parent(payload, step):
    print("running operator one")


def operator_one(payload, step):
    print("running operator two")


def operator_two(payload, step):
    print("running operator two")


STEPS = {
    "parent_step": operator_parent,
    "step_one": operator_one,
    "step_two": operator_two,
}
