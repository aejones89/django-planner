import logging

from .models import WorkflowStep
from .tasks import run_executor_task

logger = logging.getLogger(__name__)


class WorkflowPlan:
    """Context manager within which a workflow plan is defined.
    Plans are automatically executed if exiting from the root node."""

    def __init__(
        self,
        name: str,
        details: str,
        step_type: str,
        executor_name: str,
        payload: dict = None,
    ):
        self.name = name
        self.details = details
        self.step_type = step_type
        self.executor_name = executor_name
        self.payload = payload

    def __enter__(self):
        self.step = WorkflowStep.objects.create(
            name=self.name,
            details=self.details,
            payload=self.payload,
            step_type=self.step_type,
            executor_name=self.executor_name,
        )
        return self

    def add_step(self, step: WorkflowStep):
        """Add a step with the step for this workflow as a parent."""
        step.executor_name = self.executor_name
        step.parent = self.step
        step.save()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            root_node = self.step.get_root()
            root_node.delete()
            logger.warning("Exception while creating workflow. Rolling back.")
            logger.warning(exc_val, exc_info=True)

        if self.step.is_root_node():
            run_executor_task.delay(self.executor_name)
