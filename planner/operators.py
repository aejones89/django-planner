from .models import WorkflowStep


def pull_root_context(step: WorkflowStep) -> dict:
    """Get the context of the root node of a given WorkflowStep."""
    root = step.get_root()
    return root.context


def push_root_context(step: WorkflowStep, kwargs: dict):
    """Update the context of the root node of a given WorkflowStep."""
    root = step.get_root()
    for k, v in kwargs.items():
        root.context[k] = v
    root.save()
