from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import WorkflowStep, WorkflowExecutor


class WorkflowExecutorAdmin(admin.ModelAdmin):
    list_display = ["name", "status", "last_status_updated_at"]


class WorkflowStepAdmin(MPTTModelAdmin):
    list_display = ["name", "status", "created_at", "num_attempts"]
    ordering = ["-tree_id", "lft"]


admin.site.register(WorkflowStep, WorkflowStepAdmin)
admin.site.register(WorkflowExecutor, WorkflowExecutorAdmin)
