from enum import Enum


class WorkflowStepStatus:
    """Valid statuses for a WorkflowStep."""

    PLANNED = "planned"
    IN_PROGRESS = "in_progress"
    FAILED = "failed"
    SUCCESSFUL = "successful"


class WorkflowExecutorStatus:
    """Valid statuses for a WorkflowExecutor."""

    ON = "on"
    PAUSED = "paused"
    OFF = "off"
