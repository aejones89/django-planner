from django.core.signals import request_finished
from django.dispatch import receiver
from django.db.models.signals import post_save

from .models import WorkflowExecutor
from .tasks import run_executor_task


@receiver(post_save, sender=WorkflowExecutor)
def run_executor_if_turned_on(sender, instance, created, **kwargs):
    """Post save signal to run_executor if an executor is in an on state once updated."""
    if created or not instance.is_on:
        return
    run_executor_task.delay(instance.name)
    return
