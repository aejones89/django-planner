from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PlannerConfig(AppConfig):
    name = "planner"
    verbose_name = _("planner")

    def ready(self):
        import planner.signals  # noqa
