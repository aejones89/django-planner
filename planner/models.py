import logging
from django.db import models
from django.db.models.query import QuerySet
from django.utils import timezone
from django.contrib.postgres.fields import JSONField
from mptt.models import MPTTModel, TreeForeignKey


from .constants import WorkflowStepStatus, WorkflowExecutorStatus

logger = logging.getLogger(__name__)


class WorkflowStepManager(models.Manager):
    """Custom WorkflowStep model Manager."""

    def planned_steps(self, executor_name: str) -> QuerySet:
        """Return all steps which still need to be completed, correctly ordered by
        tree_id first, then by the `left_attr`.
        """
        outstanding_qs = self.filter(
            executor_name=executor_name,
            status__in=[WorkflowStepStatus.PLANNED, WorkflowStepStatus.FAILED],
        )
        ordered_qs = outstanding_qs.order_by("tree_id", "lft")
        return ordered_qs


class WorkflowStep(MPTTModel):
    """WorkflowStep Model."""

    name = models.CharField(max_length=256)
    details = models.TextField()
    parent = TreeForeignKey(
        "self", on_delete=models.CASCADE, null=True, blank=True, related_name="children"
    )
    status = models.CharField(
        choices=(
            (WorkflowStepStatus.PLANNED, "planned"),
            (WorkflowStepStatus.IN_PROGRESS, "in_progress"),
            (WorkflowStepStatus.FAILED, "failed"),
            (WorkflowStepStatus.SUCCESSFUL, "successful"),
        ),
        max_length=20,
        blank=False,
        null=False,
        default=WorkflowStepStatus.PLANNED,
    )
    num_attempts = models.IntegerField(default=0, null=False, blank=False)
    payload = JSONField(default=dict)
    context = JSONField(default=dict)
    step_type = models.CharField(max_length=32)
    executor_name = models.CharField(max_length=32)

    created_at = models.DateTimeField(auto_now_add=timezone.now)

    objects = WorkflowStepManager()

    class MPTTMeta:
        order_insertion_by = ["created_at"]

    def mark_successful(self):
        """Set the step status to successful."""
        self.status = WorkflowStepStatus.SUCCESSFUL
        self.save()

    def mark_failed(self):
        """Set the step status to failed."""
        self.status = WorkflowStepStatus.FAILED
        self.save()

    def mark_in_progress(self):
        """Set the step status to in progress."""
        self.status = WorkflowStepStatus.IN_PROGRESS
        self.save()

    def start_processing(self):
        """Set the step status to in progress and increment num_attempts by one."""
        self.status = WorkflowStepStatus.IN_PROGRESS
        self.num_attempts += 1
        self.save()

    def __str__(self) -> str:
        return f"{self.name}, State: {self.status}"


class WorkflowExecutor(models.Model):
    """WorkflowExecutor Model."""

    name = models.CharField(max_length=50, blank=False, null=False, unique=True)
    status = models.CharField(
        choices=(
            (WorkflowExecutorStatus.ON, "on"),
            (WorkflowExecutorStatus.PAUSED, "paused"),
            (WorkflowExecutorStatus.OFF, "off"),
        ),
        max_length=20,
        blank=False,
        null=False,
        default=WorkflowExecutorStatus.ON,
    )
    last_status_updated_at = models.DateTimeField(default=timezone.now)
    last_status_updated_reason = models.TextField(default="Created")

    def __str__(self) -> str:
        return f"Workflow executor {self.name} is in {self.status} state"

    def _update_status(self, new_status: str, updated_reason: str):
        """Update executor with new status and reason."""
        self.status = new_status
        self.last_status_updated_at = timezone.now()
        self.last_status_updated_reason = updated_reason
        self.save()

    def pause(self, reason: str):
        """Pause the executor with a given reason."""
        self._update_status(WorkflowExecutorStatus.PAUSED, reason)

    def resume(self, reason: str):
        """Resume the executor with a given reason."""
        self._update_status(WorkflowExecutorStatus.ON, reason)

    def turn_off(self, reason: str):
        """Turn off the executor with a given reason."""
        self._update_status(WorkflowExecutorStatus.OFF, reason)

    @property
    def is_on(self) -> bool:
        return self.status == WorkflowExecutorStatus.ON

    @property
    def is_paused(self) -> bool:
        return self.status == WorkflowExecutorStatus.PAUSED

    @property
    def is_off(self) -> bool:
        return self.status == WorkflowExecutorStatus.OFF
