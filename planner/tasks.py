from celery import shared_task

from .runner import run_executor


@shared_task
def run_executor_task(executor_name: str):
    """For a given executor name, process all steps belonging to that executor."""
    run_executor(executor_name=executor_name)
