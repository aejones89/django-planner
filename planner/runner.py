import logging
from importlib import import_module

from django.conf import settings
from django.db import transaction

from planner.constants import WorkflowStepStatus
from planner.models import WorkflowExecutor, WorkflowStep


logger = logging.getLogger(__name__)


def get_steps_dict(steps_mapping: str) -> dict:
    """Returns a dictionary mapping step types to step operators.

    Example:
        steps_mapping = 'my_module.parent.child.MY_STEPS'

        Returns MY_STEPS (dict) located in `my_module.parent.child` module.
    """
    steps_list = steps_mapping.split(".")
    steps_obj = steps_list.pop(-1)
    steps_mod = ".".join(steps_list)

    steps = getattr(import_module(steps_mod), steps_obj)
    return steps


def run_step(step: "WorkflowStep"):
    """Run the operator for a given workflow step."""
    step_type = step.step_type
    step_payload = step.payload
    steps = get_steps_dict(settings.STEPS_MAPPING)
    operator_method = steps[step_type]
    operator_method(payload=step_payload, step=step)


def run_executor(executor_name: str):
    """Process the planned steps for a given executor. Acquires database lock on the next step to be processed."""
    while WorkflowStep.objects.planned_steps(executor_name=executor_name).count() > 0:
        executor = WorkflowExecutor.objects.get(name=executor_name)
        next_step_id = (
            WorkflowStep.objects.planned_steps(executor_name=executor_name).first().id
        )
        if executor.is_on:
            with transaction.atomic():
                next_step = WorkflowStep.objects.select_for_update().get(
                    id=next_step_id
                )
                if next_step.status == WorkflowStepStatus.SUCCESSFUL:
                    continue
                try:
                    with transaction.atomic():
                        next_step.start_processing()
                        run_step(next_step)
                    next_step.mark_successful()
                    logger.info(f"Execution of step {next_step} successful")
                except Exception as e:
                    logger.error(f"Execution of step {next_step} failed", e)
                    next_step.mark_failed()
                    executor.pause(reason="Exception raised during execution")
                    break
        else:
            logger.info(f"Executor {executor_name} is not on. Run execution skipping")
            break
