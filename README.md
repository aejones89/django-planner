# Django Planner

Django planner is a dynamic workflow engine for your Django applicaation.
---

## Features

What can django-planner do for you?

* **Dynamic workflow generation** - Generate steps in your business workflow at runtime.
* **Database-backed** - Workflows are backed by postgres database transactions.
* **Asynchronous processing** - Workflows are executed asynchronously using ![Celery](http://www.celeryproject.org/)
* **Simple state management** - pass state between steps as your workflow is executed.
* **Decouple execution from definition** - Strong separation of responsiblity for readable, maintainable code.

---

## Requirements

Postgres is the only supported database backend. Celery is required as the distributed task queue.

## Quickstart

Install django-planner
```bash
$ pip3 install django-planner
```

Add planner to your installed apps

```python
INSTALLED_APPS = [
    # ...
    'planner',
]
```

And you must configure your application to use atomic requests. In your settings file add

```python
ATOMIC_REQUESTS = True
```

Define your steps and how they can be executed

```python
# mymodule.steps

PRODUCE_WIDGET = 'produce_widget'
CREATE_WIDGET = 'create_widget'
DISTRIBUTE_WIDGET = 'distribute_widget'

def produce_widget_operator(payload, step):
    print('producing widget')

def create_widget_operator(payload, step):
    print('creating_widget')

def distribute_widget_operator(payload, step):
    print('distribute widget')

STEPS = {
    PRODUCE_WIDGET: produce_widget_operator,
    CREATE_WIDGET: create_widget_operator,
    DISTRIBUTE_WIDGET: distribute_widget_operator,
}

```

Add `STEPS_MAPPING` to your Django settings file

```python
STEPS_MAPPING = 'mymodule.steps.STEPS'
```

Create the executor database object you wish to manage execution of your workflow

```python
from planner.models import WorkflowExecutor
WorkflowExecutor.objects.create('widget-executor')
```

Create your Workflow Plan

```python
from planner.engine import WorkflowPlan, WorkflowStep

from .steps import STEPS


with WorkflowPlan(
    name=f"Produce Widget",
    details='Produce a widget',
    step_type=steps.PRODUCE_WIDGET,
    executor_name='widget-executor',
) as produce_widget_workflow:
	create_step = WorkflowStep.objects.create(
        name="Create widget",
        details="Create widget",
        step_type=steps.CREATE_WIDGET,
	)
	produce_widget_workflow.add_step(create_step)
	distribute_step = WorkflowStep.objects.create(
		name="Distribute widget",
		details="Distribute widget",
		step_type=steps.DISTRIBUTE_WIDGET,
	)
	produce_widget_workflow.add_step(create_step)
```

Start your celery queue and workers. It is **important** that concurrency is set to one.

```bash
$ celery -A celery_app worker -l info --concurrency=1
```

When your Workflow Plan is executed in your application the steps will automatically be processed on the Celery queue in order.

---

### License

Django Planner is licensed unter MIT License

